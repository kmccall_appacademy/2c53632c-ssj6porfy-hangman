
class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(players = {})
    defaults = {
      guesser: nil,
      referee: nil,
      board: nil
    }
    players = defaults.merge(players)
    players.each { |k, v| instance_variable_set("@#{k}", v) }
  end

  def setup
    secret_word_length = @referee.pick_secret_word
    @guesser.register_secret_length(secret_word_length)
    @board = Array.new(secret_word_length)
  end

  def display
    printed = board.map { |place| place.nil? ? '_' : place }
    printed.join(' ')
  end

  def take_turn
    print display
    letter = guesser.guess(board)
    arr = @referee.check_guess(letter)
    @guesser.handle_response(letter, arr)
    update_board(letter, arr)
  end

  def update_board(letter, arr)
    arr.each do |idx|
      board[idx] = letter
    end
    board
  end

  def play
    setup
    take_turn until player_lost? || won?
    puts won? ? "You win! The word was #{board.join}!" : "You lost!"
  end

  def won?
    board.include?(nil) == false
  end

  def player_lost?
    guesser.chances == 10
  end
end

class HumanPlayer
  attr_reader :name, :secret_word, :register_secret_length
  attr_accessor :chances

  def initialize(name)
    @name = name
    @chances = 0
  end

  def pick_secret_word
    puts "Enter as word for the computer to guess!"
    @secret_word = gets.chomp
    secret_word.length
  end

  def register_secret_length(length)
    @register_secret_length = length
  end

  def guess(board)
    puts "\n\n"
    gets.chomp
  end

  def handle_response(letter, idxs)
    if idxs.size.zero?
      puts "'#{letter.upcase}' was a Incorrect pick!"
      @chances += 1
    else
      puts "'#{letter.upcase}' was a Correct pick"
    end
  end

  def check_guess(letter)
    puts "\nThe Computer guessed '#{letter.upcase}'\n\n"
    match_indexes = []
    secret_word.each_char.with_index do |char, idx|
      match_indexes << idx if char == letter
    end
    match_indexes
  end

end

class ComputerPlayer
  attr_reader :dictionary, :secret_word
  attr_accessor :register_secret_length, :chances, :candidate_words

  def initialize(dictionary = File.readlines("dictionary.txt"))
    @dictionary = dictionary.map(&:strip)
    @chances = 0
    @candidate_words = @dictionary.dup
  end

  def pick_secret_word
    @secret_word = dictionary.sample.strip
    secret_word.length
  end

  def check_guess(letter)
    match_indexes = []
    secret_word.each_char.with_index do |char, idx|
      match_indexes << idx if char == letter
    end
    match_indexes
  end

  def register_secret_length(length)
    @register_secret_length = length
    candidate_words.select! { |word| word.length == length }
  end

  def handle_response(letter, idxs)
    if idxs.nil? == false
      idxs.each { |idx| candidate_words.select! { |word| word[idx] == letter } }
    else
      @chances += 1
    end
    candidate_words.reject! { |word| word.count(letter) > idxs.size }
  end

  def guess(board)
    letter_count_hash = {}
    str = candidate_words.join
    letters = str.chars.uniq
    letters.each do |let|
      letter_count_hash[let] = str.count(let) unless board.include?(let)
    end
    letter_count_hash.max_by { |letter, value| value }.first
  end

end
